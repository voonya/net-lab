﻿using Lab3.Context;
using Microsoft.EntityFrameworkCore;

namespace Lab3.BLL.Tests
{
    public class TestDbContext : Lab3DbContext
    {
        public TestDbContext(DbContextOptions<Lab3DbContext> options) : base(options) { /* default */ }
    
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
        
        }

    }
}