﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL;
using BLL.Services;
using BLL.Services.Dtos;
using BLL.Services.Dtos.Ingredient;
using BLL.Services.Dtos.PriceList;
using Lab3.Context;
using Lab3.Entities;
using Lab3.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Xunit;

namespace Lab3.BLL.Tests.Tests
{
    public class DishTests
    {
        private readonly DishService _dishService;
        private readonly IngredientService _ingredientService;


        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly TestDbContext _context;

        private NewDishDto _testDish;

        public DishTests()
        {
            var cfg = new MapperConfiguration(cfg =>
            {
                // ingredient
                cfg.CreateMap<Ingredient, IngredientDto>();
                cfg.CreateMap<UpdateIngredientDto, Ingredient>();
                cfg.CreateMap<CreateIngredientDto, Ingredient>();
                cfg.CreateMap<IngredientDto, Ingredient>();
                
                // dish
                cfg.CreateMap<Dish, DishDto>();
                cfg.CreateMap<NewDishDto, Dish>();
                cfg.CreateMap<UpdateDishDto, Dish>();
                cfg.CreateMap<DishDto, Dish>();

                // pricelist
                cfg.CreateMap<PriceList, PriceListDto>();
                cfg.CreateMap<CreatePriceListDto, PriceList>();
                cfg.CreateMap<UpdatePriceListDto, PriceList>();
                cfg.CreateMap<PriceListDto, PriceList>();

                // order
                cfg.CreateMap<Order, OrderDto>();
                cfg.CreateMap<OrderDto, Order>();
                cfg.CreateMap<CreateOrderDto, Order>();
            });

            _mapper = new Mapper(cfg);
            var builder = new DbContextOptionsBuilder<Lab3DbContext>();
            builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
            builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
            builder.EnableSensitiveDataLogging();

            _context = new TestDbContext(builder.Options);
            _unitOfWork = new UnitOfWork(_context);

            _dishService = new DishService(_unitOfWork, _mapper);
            _ingredientService = new IngredientService(_unitOfWork, _mapper);
            InitialSeed();
        }
        
        [Fact]
        public async void GetAll_Success()
        {
            var expected = _testDish;
            var created = await _dishService.Create(expected);

            var all = await _dishService.GetAll();
            var geted = all.FirstOrDefault();
            
            Assert.NotNull(all);
            Assert.NotNull(geted);
            Assert.Equal(all.Count, 1);
            Assert.Equal(geted.Id, created.Id);
        }
        
        [Fact]
        public async void GetById_Success()
        {
            var expectedId = 1;
            var expected = _testDish;
            await _dishService.Create(expected);

            var actual = _dishService.GetById(1);

            Assert.NotNull(actual);
            Assert.Equal(expectedId, actual.Id);
        }

        [Fact]
        public void GetById_KeyNotFoundException()
        {
            Assert.ThrowsAsync<KeyNotFoundException>(()=>_dishService.GetById(1223));
        }
        
        [Fact]
        public async void Create_NameExist_KeyNotFoundException()
        {
            var initial = _testDish;
            await _dishService.Create(initial);
            var expected = new NewDishDto()
            {
                Name = initial.Name,
                IngredientsId = new Collection<int>(){100, 5}
            };

            Assert.ThrowsAsync<ArgumentException>(()=>_dishService.Create(expected));
        }
        
        [Fact]
        public async void Create_IngredientNotExist_KeyNotFoundException()
        {
            
            var expected = new NewDishDto()
            {
                Name = "Name",
                IngredientsId = new Collection<int>(){100, 5}
            };

            Assert.ThrowsAsync<KeyNotFoundException>(()=>_dishService.Create(expected));
        }
        
        [Fact]
        public async void Update_Success()
        {
            var initial = _testDish;
            await _dishService.Create(initial);
            
            var expected = new UpdateDishDto()
            {
                Name = "Name",
                IngredientsId = new Collection<int>(){1, 2}
            };

            var updated = await _dishService.Update(1, expected);
            
            Assert.NotNull(updated);
            
            Assert.Equal(updated.Ingredients.FirstOrDefault().Id, expected.IngredientsId.First());
            Assert.Equal(updated.Ingredients.LastOrDefault().Id, expected.IngredientsId.Last());
            Assert.Equal(updated.Name, expected.Name);
        }
        
        [Fact]
        public void Update_DishNotExist_KeyNotFoundException()
        {
            var expected = new UpdateDishDto()
            {
                Name = "Name",
                IngredientsId = new Collection<int>(){1, 2}
            };

            Assert.ThrowsAsync<KeyNotFoundException>(()=>_dishService.Update(100, expected));
        }
        
        [Fact]
        public async void Update_IngredientNotExist_KeyNotFoundException()
        {
            var initial = _testDish;
            await _dishService.Create(initial);

            var expected = new UpdateDishDto()
            {
                Name = initial.Name,
                IngredientsId = new Collection<int>(){100, 5}
            };

            Assert.ThrowsAsync<KeyNotFoundException>(()=>_dishService.Update(1, expected));
        }

        
        [Fact]
        public async void Delete_Success()
        {
            var expectedCountDeleted = 0;
            var expectedCountExisting = 1;
            var initial = _testDish;
            await _dishService.Create(initial);
        
            var dishesExisting = await _dishService.GetAll();
            Assert.Equal(expectedCountExisting, dishesExisting.Count);
        
            await _dishService.Delete(1);
        
            var dishesDeleted = await  _dishService.GetAll();
            Assert.Equal(expectedCountDeleted, dishesDeleted.Count);
        }

        [Fact]
        public void Delete_KeyNotFoundException()
        {
            Assert.ThrowsAsync<KeyNotFoundException>(() => _dishService.Delete(100));
        }


        public void InitialSeed()
        {
            var ing1 = new CreateIngredientDto()
            {
                Name = "Onion"
            };
            _ingredientService.Create(ing1);
        
            var ing2 = new CreateIngredientDto()
            {
                Name = "Carrot"
            };
            _ingredientService.Create(ing2);

            
            _testDish = new NewDishDto()
            {
                Name = "New dish",
                IngredientsId = new Collection<int>(){1, 2}
            };
        }
        
    }
}