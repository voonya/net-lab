﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BLL;
using BLL.Services;
using BLL.Services.Dtos;
using BLL.Services.Dtos.Ingredient;
using BLL.Services.Dtos.PriceList;
using Lab3.Context;
using Lab3.Entities;
using Lab3.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Xunit;

namespace Lab3.BLL.Tests.Tests
{
    public class PriceListTests
    {
        private readonly IngredientService _ingredientService;
        private readonly DishService _dishService;
        //private readonly OrderService _orderService;
        private readonly PriceListService _priceListService;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly TestDbContext _context;

        private CreatePriceListDto _testPriceList;

        public PriceListTests()
        {
            var cfg = new MapperConfiguration(cfg =>
            {
                // ingredient
                cfg.CreateMap<Ingredient, IngredientDto>();
                cfg.CreateMap<UpdateIngredientDto, Ingredient>();
                cfg.CreateMap<CreateIngredientDto, Ingredient>();
                cfg.CreateMap<IngredientDto, Ingredient>();
                
                // dish
                cfg.CreateMap<Dish, DishDto>();
                cfg.CreateMap<NewDishDto, Dish>();
                cfg.CreateMap<UpdateDishDto, Dish>();
                cfg.CreateMap<DishDto, Dish>();

                // pricelist
                cfg.CreateMap<PriceList, PriceListDto>();
                cfg.CreateMap<CreatePriceListDto, PriceList>();
                cfg.CreateMap<UpdatePriceListDto, PriceList>();
                cfg.CreateMap<PriceListDto, PriceList>();

                // order
                cfg.CreateMap<Order, OrderDto>();
                cfg.CreateMap<OrderDto, Order>();
                cfg.CreateMap<CreateOrderDto, Order>();
            });

            _mapper = new Mapper(cfg);
            var builder = new DbContextOptionsBuilder<Lab3DbContext>();
            builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
            builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
            builder.EnableSensitiveDataLogging();

            _context = new TestDbContext(builder.Options);
            _unitOfWork = new UnitOfWork(_context);

            _ingredientService = new IngredientService(_unitOfWork, _mapper);
            _dishService = new DishService(_unitOfWork, _mapper);
            _priceListService = new PriceListService(_unitOfWork, _mapper);

            InitialSeed();
        }

        [Fact]
        public async void GetByID_Success()
        {
            var newPriceList = _testPriceList;
            var created = await _priceListService.Create(newPriceList);
            
            var geted = await _priceListService.GetById(1);

            Assert.NotNull(geted);
            Assert.Equal(created.Id, geted.Id);
        }
        
        [Fact]
        public async void GetByID_IdNotExist_KeyNotFoundException()
        {
            Assert.ThrowsAsync<KeyNotFoundException>(()=>_priceListService.GetById(1223));
        }
        
        [Fact]
        public async void GetAll_Success()
        {
            var expected = _testPriceList;
            var created = await _priceListService.Create(expected);

            var all = await _priceListService.GetAll();
            var geted = all.FirstOrDefault();
            
            Assert.NotNull(all);
            Assert.NotNull(geted);
            Assert.Equal(all.Count, 1);
            Assert.Equal(geted.Id, created.Id);
        }
        
        [Fact]
        public async void Create_Success()
        {
            var expected = _testPriceList;
            var created = await _priceListService.Create(expected);
            
            Assert.NotNull(created);
            
            Assert.Equal(created.Dish.Id, expected.DishId);
            Assert.Equal(created.Weight, expected.Weight);
            Assert.Equal(created.Price, expected.Price);
        }
        
        [Fact]
        public async void Create_DishNotExist_KeyNotFoundException()
        {
            var newPriceList = _testPriceList;

            newPriceList.DishId = 1000;

            Assert.ThrowsAsync<KeyNotFoundException>(() => _priceListService.Create(newPriceList));
        }
        
        [Fact]
        public async void Update_Success()
        {
            var newPriceList = _testPriceList;
            var created = await _priceListService.Create(newPriceList);
            
            var geted = await _priceListService.GetById(created.Id);
            
            var updatePricelist = new UpdatePriceListDto()
            {
                DishId = 2,
                Weight = 5,
                Price = 6
            };

            var updated = await _priceListService.Update(geted.Id, updatePricelist);
            Assert.Equal(updated.Dish.Id, updatePricelist.DishId);
            Assert.Equal(updated.Weight, updatePricelist.Weight);
            Assert.Equal(updated.Price, updatePricelist.Price);
        }
        
        [Fact]
        public async void Update_DishNotExist_KeyNotFoundException()
        {
            var newPriceList = _testPriceList;
            var created = await _priceListService.Create(newPriceList);
            
            var geted = await _priceListService.GetById(created.Id);
            
            var updatePricelist = new UpdatePriceListDto()
            {
                DishId = 1000,
                Weight = 5,
                Price = 6
            };

            Assert.ThrowsAsync<KeyNotFoundException>(()=>_priceListService.Update(geted.Id, updatePricelist));
        }
        
        [Fact]
        public async void Update_NotExist_KeyNotFoundException()
        {
            var newPricelist = new UpdatePriceListDto()
            {
                DishId = 1,
                Weight = 5,
                Price = 6
            };

            Assert.ThrowsAsync<KeyNotFoundException>(()=>_priceListService.Update(1000, newPricelist));
        }
        
        [Fact]
        public async void Delete_Success()
        {
            var expectedCountExisting = 0;
            var initial = _testPriceList;
            
            var created = await _priceListService.Create(initial);
            
            var deleted = await _priceListService.Delete(1);
        
            var priceListsExisting = await  _priceListService.GetAll();
            
            Assert.Equal(expectedCountExisting, priceListsExisting.Count);
            Assert.Equal(deleted.Id, created.Id);
        }

        [Fact]
        public void Delete_KeyNotFoundException()
        {
            Assert.ThrowsAsync<KeyNotFoundException>(() => _priceListService.Delete(100));
        }
        public void InitialSeed()
        {
            var ing1 = new CreateIngredientDto()
            {
                Name = "Onion"
            };
            _ingredientService.Create(ing1);
        
            var ing2 = new CreateIngredientDto()
            {
                Name = "Carrot"
            };
            _ingredientService.Create(ing2);

            var ing3 = new CreateIngredientDto()
            {
                Name = "Pumlin"
            };
            
            _ingredientService.Create(ing3);

            var dish1 = new NewDishDto()
            {
                Name = "Dish 1",
                IngredientsId = new List<int>() { 1, 2 }
            };
            _dishService.Create(dish1);
            
            var dish2 = new NewDishDto()
            {
                Name = "Dish 2",
                IngredientsId = new List<int>() { 2, 3 }
            };
            _dishService.Create(dish2);
            
            _testPriceList = new CreatePriceListDto()
            {
                DishId = 1,
                Price = 10,
                Weight = 100
            };
        }
    }
}