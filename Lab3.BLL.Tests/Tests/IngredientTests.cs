﻿using System;
using AutoMapper;
using BLL;
using BLL.Services;
using BLL.Services.Dtos;
using BLL.Services.Dtos.Ingredient;
using BLL.Services.Dtos.PriceList;
using Lab3.Context;
using Lab3.Entities;
using Lab3.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Xunit;
using System.Linq;

namespace Lab3.BLL.Tests.Tests
{
    public class IngredientTests
    {
        private readonly IngredientService _ingredientService;
        //private readonly DishService _dishService;
        //private readonly OrderService _orderService;
        //private readonly PriceList _priceList;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly TestDbContext _context;


        public IngredientTests()
        {
            var cfg = new MapperConfiguration(cfg =>
            {
                // ingredient
                cfg.CreateMap<Ingredient, IngredientDto>();
                cfg.CreateMap<UpdateIngredientDto, Ingredient>();
                cfg.CreateMap<CreateIngredientDto, Ingredient>();
                cfg.CreateMap<IngredientDto, Ingredient>();
                
                // dish
                cfg.CreateMap<Dish, DishDto>();
                cfg.CreateMap<NewDishDto, Dish>();
                cfg.CreateMap<UpdateDishDto, Dish>();
                cfg.CreateMap<DishDto, Dish>();

                // pricelist
                cfg.CreateMap<PriceList, PriceListDto>();
                cfg.CreateMap<CreatePriceListDto, PriceList>();
                cfg.CreateMap<UpdatePriceListDto, PriceList>();
                cfg.CreateMap<PriceListDto, PriceList>();

                // order
                cfg.CreateMap<Order, OrderDto>();
                cfg.CreateMap<OrderDto, Order>();
                cfg.CreateMap<CreateOrderDto, Order>();
            });

            _mapper = new Mapper(cfg);
            var builder = new DbContextOptionsBuilder<Lab3DbContext>();
            builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
            builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
            builder.EnableSensitiveDataLogging();

            _context = new TestDbContext(builder.Options);
            _unitOfWork = new UnitOfWork(_context);

            _ingredientService = new IngredientService(_unitOfWork, _mapper);

        }
        
        [Fact]
        public async void GetById_Success()
        {
            var expected = new CreateIngredientDto()
            {
                Name = "Milk without laktose"
            };

            var created = await _ingredientService.Create(expected);
            var geted = await _ingredientService.GetById(created.Id);

            Assert.NotNull(geted);

            Assert.Equal(created.Name, geted.Name);
            Assert.Equal(created.Id, geted.Id);
        }
        
        [Fact]
        public async void GetAll_Success()
        {
            var expected = new CreateIngredientDto()
            {
                Name = "Milk without laktose"
            };

            var created = await _ingredientService.Create(expected);
            var geted = await _ingredientService.GetAll();

            Assert.NotNull(geted);

            Assert.Equal(geted.Count, 1);
            Assert.Equal(geted.First().Id, created.Id);
        }
        
        [Fact]
        public async void Create_Success()
        {
            var expected = new CreateIngredientDto()
            {
                Name = "Milk without laktose"
            };

            await _ingredientService.Create(expected);
            var ingredients = await _ingredientService.GetAll();
            var actual = ingredients.FirstOrDefault();

            Assert.NotNull(actual);

            Assert.Equal(expected.Name, actual.Name);

        }
        
        [Fact]
        public void CreateExisting_ArgumentException()
        {
            var ingredient1 = new CreateIngredientDto()
            {
                Name = "Огірок"
            };

            _ingredientService.Create(ingredient1);

            var ingredient2 = new CreateIngredientDto()
            {
                Name = "Огірок"
            };

            Assert.ThrowsAsync<ArgumentException>(async () => await _ingredientService.Create(ingredient2));
        }
        
        [Fact]
        public void Update_NameExist_ArgumentException()
        {
            var ingredient1 = new CreateIngredientDto()
            {
                Name = "Огірок"
            };

            _ingredientService.Create(ingredient1);

            var ingredient2 = new CreateIngredientDto()
            {
                Name = "Oгірок 1"
            };
            
            _ingredientService.Create(ingredient2);
            
            var ingredient3 = new UpdateIngredientDto()
            {
                Name = "Oгірок 1"
            };

            Assert.ThrowsAsync<ArgumentException>(async () => await _ingredientService.Update(1, ingredient3));
        }

        [Fact]
        public async void Delete_Success()
        {
            var expectedCountDeleted = 0;
            var expectedCountExisting = 1;
            var ingredient = new CreateIngredientDto()
            {
                Name = "Огірок"
            };
            var createdIngredient = await _ingredientService.Create(ingredient);
        
            var ingredientsExisting = await _ingredientService.GetAll();
            Assert.Equal(expectedCountExisting, ingredientsExisting.Count);
        
            await _ingredientService.Delete(createdIngredient.Id);
        
            var tasDeleted = await _ingredientService.GetAll();
            Assert.Equal(expectedCountDeleted, tasDeleted.Count);
        }

    }
    
    
}