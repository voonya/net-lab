﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public interface IOrderService
    {
        Task<OrderDto> GetById(int id);
        Task<ICollection<OrderDto>> GetAll();
        Task<OrderDto> Create(CreateOrderDto newOrder);
        Task<OrderDto> Update(int id, UpdateOrderDto newOrder);
        Task<OrderDto> Delete(int id);
    }
}