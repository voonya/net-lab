﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Services.Dtos.PriceList;

namespace BLL.Services
{
    public interface IPriceListService
    {
        Task<PriceListDto> GetById(int id);
        Task<ICollection<PriceListDto>> GetAll();
        Task<PriceListDto> Create(CreatePriceListDto newPriceList);
        Task<PriceListDto> Update(int id, UpdatePriceListDto newPriceList);
        Task<PriceListDto> Delete(int id);
    }
}