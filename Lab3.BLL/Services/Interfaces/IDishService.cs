﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Services.Dtos;

namespace BLL.Services
{
    public interface IDishService
    {
        Task<DishDto> GetById(int id);
        Task<ICollection<DishDto>> GetAll();
        Task<DishDto> Create(NewDishDto newDish);
        Task<DishDto> Update(int id, UpdateDishDto newDish);
        Task<DishDto> Delete(int id);
    }
}