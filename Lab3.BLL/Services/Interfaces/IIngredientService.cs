﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Services.Dtos.Ingredient;

namespace BLL.Services
{
    public interface IIngredientService
    {
        Task<IngredientDto> GetById(int id);
        Task<ICollection<IngredientDto>> GetAll();
        Task<IngredientDto> Create(CreateIngredientDto newIngredient);
        Task<IngredientDto> Update(int id, UpdateIngredientDto newIngredient);
        Task<IngredientDto> Delete(int id);
    }
}