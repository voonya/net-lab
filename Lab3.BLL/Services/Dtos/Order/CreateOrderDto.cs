﻿using System.Collections.Generic;

namespace BLL
{
    public class CreateOrderDto
    {
        public ICollection<int> ItemsId { get; set; }
    }
}