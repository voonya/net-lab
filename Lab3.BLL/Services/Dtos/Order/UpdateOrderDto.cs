﻿using System.Collections.Generic;

namespace BLL
{
    public class UpdateOrderDto
    {
        public ICollection<int> ItemsId { get; set; }
    }
}