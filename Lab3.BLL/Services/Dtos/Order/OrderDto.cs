﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using BLL.Services.Dtos.PriceList;
using Lab3.Entities;

namespace BLL
{
    public class OrderDto
    {
        public int Id { get; set; }
        public ICollection<PriceListDto> Items { get; set; }
    }
}