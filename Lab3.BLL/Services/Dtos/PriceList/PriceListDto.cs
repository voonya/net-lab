﻿using Lab3.Entities;

namespace BLL.Services.Dtos.PriceList
{
    public class PriceListDto
    {
        public int Id { get; set; }
        public DishDto Dish { get; set; }
        public int Weight { get; set; }
        public decimal Price { get; set; }
    }
}