﻿namespace BLL.Services.Dtos.PriceList
{
    public class CreatePriceListDto
    {
        public int DishId { get; set; }
        public int Weight { get; set; }
        public decimal Price { get; set; }
    }
}