﻿namespace BLL.Services.Dtos.PriceList
{
    public class UpdatePriceListDto
    { 
        public int Weight { get; set; }
        public decimal Price { get; set; }
    }
}