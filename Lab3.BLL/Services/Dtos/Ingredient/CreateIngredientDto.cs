﻿namespace BLL.Services.Dtos.Ingredient
{
    public class CreateIngredientDto
    {
        public string Name { get; set; }
    }
}