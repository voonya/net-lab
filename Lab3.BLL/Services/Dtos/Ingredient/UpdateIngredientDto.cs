﻿namespace BLL.Services.Dtos.Ingredient
{
    public class UpdateIngredientDto
    {
        public string Name { get; set; }
    }
}