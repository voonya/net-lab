﻿using System.Collections.Generic;
using BLL.Services.Dtos.Ingredient;

namespace BLL.Services.Dtos
{
    public class NewDishDto
    {
        public string Name { get; set; }
        public ICollection<int> IngredientsId { get; set; }
    }
}