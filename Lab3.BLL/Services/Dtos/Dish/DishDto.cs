﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using BLL.Services.Dtos.Ingredient;
using Lab3.Entities;

namespace BLL.Services.Dtos
{
    public class DishDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<IngredientDto> Ingredients { get; set; }
    }
}