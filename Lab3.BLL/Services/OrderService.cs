﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Services.Abstract;
using BLL.Services.Dtos;
using Lab3.Entities;
using Lab3.Interfaces;

namespace BLL.Services
{
    public class OrderService:  BaseService, IOrderService
    {
        public OrderService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper){}

        public async Task<OrderDto> GetById(int id)
        {
            var orderEntity = await UnitOfWork.OrdersRepository.GetById(id) ??
                                throw new KeyNotFoundException($"Order with id: {id} was not found!");

            return Mapper.Map<OrderDto>(orderEntity);
        }

        public async Task<ICollection<OrderDto>> GetAll()
        {
            var orders = await UnitOfWork.OrdersRepository.GetAll();

            return Mapper.Map<ICollection<OrderDto>>(orders);
        }

        public async Task<OrderDto> Create(CreateOrderDto newOrder)
        {
            var orderEntity = Mapper.Map<Order>(newOrder);

            orderEntity.Items = new Collection<PriceList>();
  
            foreach (var id in newOrder.ItemsId)
            {
                var priceListEntity = await UnitOfWork.PriceListRepository.GetById(id) ??
                                 throw new KeyNotFoundException($"Price list with id: {id} was not found!");

                orderEntity.Items.Add(priceListEntity);
            }
            
            await UnitOfWork.OrdersRepository.Add(orderEntity);
            await UnitOfWork.SaveChangesAsync();

            return Mapper.Map<OrderDto>(orderEntity);
        }

        public async Task<OrderDto> Update(int id, UpdateOrderDto newOrder)
        {
            var orderEntity = await UnitOfWork.OrdersRepository.GetById(id) ??
                                throw new KeyNotFoundException($"Order with id: {id} was not found!");

            orderEntity.Items.Clear();
            
            foreach (var itemId in newOrder.ItemsId)
            {
               var priceListEntity = await UnitOfWork.PriceListRepository.GetById(itemId) ??
                                       throw new KeyNotFoundException($"Ingredient with id: {itemId} was not found!");
               orderEntity.Items.Add(priceListEntity);
            }

            await UnitOfWork.OrdersRepository.Update(orderEntity);
            await UnitOfWork.SaveChangesAsync();

            return Mapper.Map<OrderDto>(orderEntity);
        }

        public async Task<OrderDto> Delete(int id)
        {
            var orderEntity = await UnitOfWork.OrdersRepository.GetById(id) ??
                             throw new KeyNotFoundException($"Order with id: {id} was not found!");

            await UnitOfWork.OrdersRepository.Delete(orderEntity);
            await UnitOfWork.SaveChangesAsync();
            
            return Mapper.Map<OrderDto>(orderEntity);
        }    
    }
}