﻿

using AutoMapper;
using Lab3.Interfaces;

namespace BLL.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly IUnitOfWork UnitOfWork;
        private protected readonly IMapper Mapper;

        public BaseService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            UnitOfWork = unitOfWork;
            Mapper = mapper;
        }
    }
}