﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Services.Abstract;
using BLL.Services.Dtos;
using BLL.Services.Dtos.PriceList;
using Lab3.Entities;
using Lab3.Interfaces;

namespace BLL.Services
{
    public class PriceListService : BaseService, IPriceListService
    {
        public PriceListService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper){}
        
        public async Task<PriceListDto> GetById(int id)
        {
            var priceListEntity = await UnitOfWork.PriceListRepository.GetById(id) ??
                             throw new KeyNotFoundException($"Price list with id: {id} was not found!");

            return Mapper.Map<PriceListDto>(priceListEntity);
        }

        public async Task<ICollection<PriceListDto>> GetAll()
        {
            var priceLists = await UnitOfWork.PriceListRepository.GetAll();

            return Mapper.Map<ICollection<PriceListDto>>(priceLists);
        }

        public async Task<PriceListDto> Create(CreatePriceListDto newPriceList)
        {
            var priceListEntity = Mapper.Map<PriceList>(newPriceList);
            
            var dishEntity = await UnitOfWork.DishRepository.GetById(newPriceList.DishId) ??
                             throw new KeyNotFoundException($"Dish with id: {newPriceList.DishId} was not found!");

            await UnitOfWork.PriceListRepository.Add(priceListEntity);
            await UnitOfWork.SaveChangesAsync();

            return Mapper.Map<PriceListDto>(priceListEntity);
        }

        public async Task<PriceListDto> Update(int id, UpdatePriceListDto newPriceList)
        {
            var priceListEntity = await UnitOfWork.PriceListRepository.GetById(id) ??
                                  throw new KeyNotFoundException($"Price list with id: {id} was not found!");
            
            var updatePriceList = Mapper.Map<PriceList>(newPriceList);
            
            priceListEntity.Weight = updatePriceList.Weight;
            
            priceListEntity.Price = updatePriceList.Price;
            
            await UnitOfWork.PriceListRepository.Update(priceListEntity);
            await UnitOfWork.SaveChangesAsync();

            return Mapper.Map<PriceListDto>(priceListEntity);
        }

        public async Task<PriceListDto> Delete(int id)
        {
            var priceListEntity = await UnitOfWork.PriceListRepository.GetById(id) ??
                             throw new KeyNotFoundException($"Price list with id: {id} was not found!");

            await UnitOfWork.PriceListRepository.Delete(priceListEntity);
            await UnitOfWork.SaveChangesAsync();
            
            return Mapper.Map<PriceListDto>(priceListEntity);
        }
    }
}