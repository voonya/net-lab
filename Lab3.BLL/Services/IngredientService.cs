﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Services.Abstract;
using BLL.Services.Dtos;
using BLL.Services.Dtos.Ingredient;
using Lab3.Entities;
using Lab3.Interfaces;

namespace BLL.Services
{
    public class IngredientService: BaseService, IIngredientService
    {
        public IngredientService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper){}

        public async Task<IngredientDto> GetById(int id)
        {
            var ingredientEntity = await UnitOfWork.IngredientRepository.GetById(id) ??
                                    throw new KeyNotFoundException($"Ingredient with id: {id} was not found!");

            return Mapper.Map<IngredientDto>(ingredientEntity);
        }

        public async Task<ICollection<IngredientDto>> GetAll()
        {
            var ingredients = await UnitOfWork.IngredientRepository.GetAll();

            return Mapper.Map<ICollection<IngredientDto>>(ingredients);
        }

        public async Task<IngredientDto> Create(CreateIngredientDto newIngredient)
        {
            var ingredientEntity = Mapper.Map<Ingredient>(newIngredient);
            
            var ingredient = await UnitOfWork.IngredientRepository.GetByName(newIngredient.Name); 
            
            if (ingredient is not null)
            {
                throw new ArgumentException("Ingredient already exist");
            }
            await UnitOfWork.IngredientRepository.Add(ingredientEntity);
            await UnitOfWork.SaveChangesAsync();

            return Mapper.Map<IngredientDto>(ingredientEntity);
        }

        public async Task<IngredientDto> Update(int id, UpdateIngredientDto newIngredient)
        {
            var ingredientEntity = await UnitOfWork.IngredientRepository.GetById(id) ??
                             throw new KeyNotFoundException($"Ingredient with id: {id} was not found!");

            var updateIngredient = Mapper.Map<Ingredient>(newIngredient);

            ingredientEntity.Name = updateIngredient.Name;

            await UnitOfWork.IngredientRepository.Update(ingredientEntity);
            await UnitOfWork.SaveChangesAsync();

            return Mapper.Map<IngredientDto>(ingredientEntity);
        }

        public async Task<IngredientDto> Delete(int id)
        {
            var ingredientEntity = await UnitOfWork.IngredientRepository.GetById(id) ??
                             throw new KeyNotFoundException($"Ingredient with id: {id} was not found!");

            await UnitOfWork.IngredientRepository.Delete(ingredientEntity);
            await UnitOfWork.SaveChangesAsync();
            
            return Mapper.Map<IngredientDto>(ingredientEntity);
        }
    }
}