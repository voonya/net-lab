﻿using AutoMapper;
using Lab3.Entities;

namespace BLL
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<Order, OrderDto>();
            CreateMap<OrderDto, Order>();
            CreateMap<CreateOrderDto, Order>();
        }
    }
}