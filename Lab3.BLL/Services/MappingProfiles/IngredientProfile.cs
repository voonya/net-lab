﻿using AutoMapper;
using BLL.Services.Dtos;
using BLL.Services.Dtos.Ingredient;
using Lab3.Entities;

namespace BLL.Services.MappingProfiles
{
    public class IngredientProfile : Profile
    {
        public IngredientProfile()
        {
            CreateMap<Ingredient, IngredientDto>();
            CreateMap<UpdateIngredientDto, Ingredient>();
            CreateMap<CreateIngredientDto, Ingredient>();
            CreateMap<IngredientDto, Ingredient>();
        }
    }
}