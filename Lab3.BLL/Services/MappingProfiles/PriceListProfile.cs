﻿using AutoMapper;
using BLL.Services.Dtos;
using BLL.Services.Dtos.Ingredient;
using BLL.Services.Dtos.PriceList;
using Lab3.Entities;

namespace BLL.Services.MappingProfiles
{
    public class PriceListProfile : Profile
    {
        public PriceListProfile()
        {
            CreateMap<PriceList, PriceListDto>();
            CreateMap<CreatePriceListDto, PriceList>();
            CreateMap<UpdatePriceListDto, PriceList>();
            CreateMap<PriceListDto, PriceList>();
        }
    }
}