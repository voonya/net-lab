﻿using AutoMapper;
using BLL.Services.Dtos;
using Lab3.Entities;

namespace BLL
{
    public class DishProfile : Profile
    {
        public DishProfile()
        {
            CreateMap<Dish, DishDto>();
            CreateMap<NewDishDto, Dish>();
            CreateMap<UpdateDishDto, Dish>();
            CreateMap<DishDto, Dish>();
        }
        
    }
}