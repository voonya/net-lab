﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Services.Abstract;
using BLL.Services.Dtos;
using Lab3.Entities;
using Lab3.Interfaces;
namespace BLL.Services
{
    public class DishService : BaseService, IDishService
    {
        public DishService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper){}

        public async Task<DishDto> GetById(int id)
        {
            var dishEntity = await UnitOfWork.DishRepository.GetById(id) ??
                                throw new KeyNotFoundException($"Dish with id: {id} was not found!");

            return Mapper.Map<DishDto>(dishEntity);
        }

        public async Task<ICollection<DishDto>> GetAll()
        {
            var dishes = await UnitOfWork.DishRepository.GetAll();

            return Mapper.Map<ICollection<DishDto>>(dishes);
        }

        public async Task<DishDto> Create(NewDishDto newDish)
        {
            var dishEntity = Mapper.Map<Dish>(newDish);
            
            var dish = await UnitOfWork.DishRepository.GetByName(newDish.Name);
            if (dish is not null)
            {
                throw new ArgumentException("Dish already exist");
            }



            dishEntity.Ingredients = new Collection<Ingredient>();

            foreach (var id in newDish.IngredientsId)
            {
                var ingredientEntity = await UnitOfWork.IngredientRepository.GetById(id) ??
                                 throw new KeyNotFoundException($"Ingredient with id: {id} was not found!");
                Console.WriteLine(ingredientEntity.Id);
                dishEntity.Ingredients.Add(ingredientEntity);
            }
            
            await UnitOfWork.DishRepository.Add(dishEntity);
            await UnitOfWork.SaveChangesAsync();

            return Mapper.Map<DishDto>(dishEntity);
        }

        public async Task<DishDto> Update(int id, UpdateDishDto newDish)
        {
            var dishEntity = await UnitOfWork.DishRepository.GetById(id) ??
                                throw new KeyNotFoundException($"Dish with id: {id} was not found!");
            

            dishEntity.Name = newDish.Name;

            dishEntity.Ingredients.Clear();
            foreach (var ingredientId in newDish.IngredientsId)
            {
               var ingredientEntity = await UnitOfWork.IngredientRepository.GetById(ingredientId) ??
                                       throw new KeyNotFoundException($"Ingredient with id: {ingredientId} was not found!");
                dishEntity.Ingredients.Add(ingredientEntity);
            }

            await UnitOfWork.DishRepository.Update(dishEntity);
            await UnitOfWork.SaveChangesAsync();

            return Mapper.Map<DishDto>(dishEntity);
        }

        public async Task<DishDto> Delete(int id)
        {
            var dishEntity = await UnitOfWork.DishRepository.GetById(id) ??
                             throw new KeyNotFoundException($"Dish with id: {id} was not found!");

            await UnitOfWork.DishRepository.Delete(dishEntity);
            await UnitOfWork.SaveChangesAsync();
            
            return Mapper.Map<DishDto>(dishEntity);
        }
    }
}