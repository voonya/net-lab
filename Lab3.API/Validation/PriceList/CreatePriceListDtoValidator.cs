﻿using BLL.Services.Dtos.Ingredient;
using BLL.Services.Dtos.PriceList;
using FluentValidation;

namespace Lab3.API.Validation.PriceList
{
    public class CreatePriceListDtoValidator : AbstractValidator<CreatePriceListDto>
    {
        public CreatePriceListDtoValidator()
        {
            RuleFor(a => a.DishId)
                .NotNull()
                .WithMessage("Dish id should not be empty");
            
            RuleFor(a => a.Weight)
                .GreaterThan(0)
                .WithMessage("Weight should be greater than 0");
            
            RuleFor(a => a.Price)
                .GreaterThan(0)
                .WithMessage("Weight should be greater than 0");
        }
    }
}