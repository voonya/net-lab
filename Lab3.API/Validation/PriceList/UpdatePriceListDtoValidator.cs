﻿using BLL.Services.Dtos.Ingredient;
using BLL.Services.Dtos.PriceList;
using FluentValidation;

namespace Lab3.API.Validation.PriceList
{
    public class UpdatePriceListDtoValidator : AbstractValidator<UpdatePriceListDto>
    {
        public UpdatePriceListDtoValidator()
        {
            RuleFor(a => a.Weight)
                .GreaterThan(0)
                .WithMessage("Weight should be greater than 0");
            
            RuleFor(a => a.Price)
                .GreaterThan(0)
                .WithMessage("Weight should be greater than 0");
        }
    }
}