﻿using BLL;
using BLL.Services.Dtos;
using FluentValidation;

namespace Lab3.API.Validation.Dish
{
    public class CreateOrderDtoValidator :  AbstractValidator<CreateOrderDto>
    {
        public CreateOrderDtoValidator()
        {
            RuleFor(a => a.ItemsId)
                .NotEmpty()
                .WithMessage("Dish should contain ingredients");
        }
    }
}