﻿using BLL;
using BLL.Services.Dtos.Ingredient;
using FluentValidation;

namespace Lab3.API.Validation.Ingredient
{
    public class UpdateIngredientDtoValidator : AbstractValidator<UpdateIngredientDto>
    {
        public UpdateIngredientDtoValidator()
        {
            RuleFor(a => a.Name)
                .NotEmpty()
                .WithMessage("Name should not be empty");
        }
    }
}