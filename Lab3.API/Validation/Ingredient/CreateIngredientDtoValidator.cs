﻿using BLL.Services.Dtos.Ingredient;
using FluentValidation;

namespace Lab3.API.Validation.Ingredient
{
    public class CreateIngredientDtoValidator : AbstractValidator<CreateIngredientDto>
    {
        public CreateIngredientDtoValidator()
        {
            RuleFor(a => a.Name)
                .NotEmpty()
                .WithMessage("Name should not be empty");
        }
    }
}