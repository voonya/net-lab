﻿using BLL.Services.Dtos;
using FluentValidation;

namespace Lab3.API.Validation.Dish
{
    public class UpdateDishDtoValidator : AbstractValidator<UpdateDishDto>
    {
        public UpdateDishDtoValidator()
        {
            RuleFor(a => a.Name)
                .NotEmpty()
                .WithMessage("Name should not be empty");
            RuleFor(a => a.IngredientsId)
                .NotEmpty()
                .WithMessage("Dish should contain ingredients");
        }
    }
}