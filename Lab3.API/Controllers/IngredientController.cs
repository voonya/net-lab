﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Services;
using BLL.Services.Dtos.Ingredient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Http;

namespace Lab3.API.Controllers
{
    [ApiController]
    [Route("ingredient")]
    public class IngredientController : ControllerBase
    {
        private readonly IIngredientService _ingredientTaskService;

        public IngredientController(IIngredientService ingredientTaskService)
        {
            _ingredientTaskService = ingredientTaskService;
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<IngredientDto>> GetById(int id)
        {
            return Ok(await _ingredientTaskService.GetById(id));
        }
        
        [HttpGet]
        public async Task<ActionResult<ICollection<IngredientDto>>> GetAll()
        {
            return Ok(await _ingredientTaskService.GetAll());
        }
        
        [HttpPost]
        public async Task<ActionResult<IngredientDto>> Create([FromBody] CreateIngredientDto ingredient)
        {
            return StatusCode(StatusCodes.Status201Created, await _ingredientTaskService.Create(ingredient));
        }
        
        [HttpDelete("{id}")]
        public async Task<ActionResult<IngredientDto>> Delete(int id)
        {
            return Ok(await _ingredientTaskService.Delete(id));
        }
        
        [HttpPut("{id}")]
        public async Task<ActionResult<IngredientDto>> Update(int id, [FromBody] UpdateIngredientDto ingredient)
        {
            return Ok(await _ingredientTaskService.Update(id, ingredient));
        }
    }
}