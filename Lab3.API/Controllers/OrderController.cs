﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL;
using BLL.Services;
using BLL.Services.Dtos;
using BLL.Services.Dtos.Ingredient;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
namespace Lab3.API.Controllers
{
    [ApiController]
    [Route("order")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<OrderDto>> GetById(int id)
        {
            return Ok(await _orderService.GetById(id));
        }
        
        [HttpGet]
        public async Task<ActionResult<ICollection<OrderDto>>> GetAll()
        {
            return Ok(await _orderService.GetAll());
        }
        
        [HttpPost]
        public async Task<ActionResult<OrderDto>> Create([FromBody] CreateOrderDto order)
        {
            return StatusCode(StatusCodes.Status201Created, await _orderService.Create(order));
        }
        
        [HttpDelete("{id}")]
        public async Task<ActionResult<OrderDto>> Delete(int id)
        {
            return Ok(await _orderService.Delete(id));
        }
        
        [HttpPut("{id}")]
        public async Task<ActionResult<OrderDto>> Update(int id, [FromBody] UpdateOrderDto order)
        {
            return Ok(await _orderService.Update(id, order));
        }
    }
}