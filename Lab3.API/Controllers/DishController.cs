﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Services;
using BLL.Services.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Lab3.API.Controllers
{
    [ApiController]
    [Route("dish")]
    public class DishController : ControllerBase
    {
        private readonly IDishService _dishService;

        public DishController(IDishService dishService)
        {
            _dishService = dishService;
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<DishDto>> GetById(int id)
        {
            return Ok(await _dishService.GetById(id));
        }
        
        [HttpGet]
        public async Task<ActionResult<ICollection<DishDto>>> GetAll()
        {
            return Ok(await _dishService.GetAll());
        }
        
        [HttpPost]
        public async Task<ActionResult<DishDto>> Create([FromBody] NewDishDto dish)
        {
            return StatusCode(StatusCodes.Status201Created, await _dishService.Create(dish));
        }
        
        [HttpDelete("{id}")]
        public async Task<ActionResult<DishDto>> Delete(int id)
        {
            return Ok(await _dishService.Delete(id));
        }
        
        [HttpPut("{id}")]
        public async Task<ActionResult<DishDto>> Update(int id, [FromBody] UpdateDishDto dish)
        {
            return Ok(await _dishService.Update(id, dish));
        }
    }
}