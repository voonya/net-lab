﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Services;
using BLL.Services.Dtos.PriceList;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Lab3.API.Controllers
{
    [ApiController]
    [Route("pricelist")]
    public class PriceListController : ControllerBase
    {
        private readonly IPriceListService _priceListTaskService;

        public PriceListController(IPriceListService priceListTaskService)
        {
            _priceListTaskService = priceListTaskService;
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<PriceListDto>> GetById(int id)
        {
            return Ok(await _priceListTaskService.GetById(id));
        }
        
        [HttpGet]
        public async Task<ActionResult<ICollection<PriceListDto>>> GetAll()
        {
            return Ok(await _priceListTaskService.GetAll());
        }
        
        [HttpPost]
        public async Task<ActionResult<PriceListDto>> Create([FromBody] CreatePriceListDto priceList)
        {
            return StatusCode(StatusCodes.Status201Created, await _priceListTaskService.Create(priceList));
        }
        
        [HttpDelete("{id}")]
        public async Task<ActionResult<PriceListDto>> Delete(int id)
        {
            return Ok(await _priceListTaskService.Delete(id));
        }
        
        [HttpPut("{id}")]
        public async Task<ActionResult<PriceListDto>> Update(int id, [FromBody] UpdatePriceListDto priceList)
        {
            return Ok(await _priceListTaskService.Update(id, priceList));
        }
    }
}