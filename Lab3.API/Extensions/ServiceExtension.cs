﻿using System.Reflection;
using BLL;
using BLL.Services;
using BLL.Services.Dtos;
using BLL.Services.Dtos.Ingredient;
using BLL.Services.Dtos.PriceList;
using BLL.Services.MappingProfiles;
using FluentValidation;
using Lab3.API.Validation.Dish;
using Lab3.API.Validation.Ingredient;
using Lab3.API.Validation.PriceList;
using Lab3.Context;
using Lab3.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Lab3.API.Extensions
{
    public static class ServiceExtension
    {
        public static void RegisterCustomServices(this IServiceCollection services)
    {
        services.AddScoped<IDishService, DishService>();
        services.AddScoped<IIngredientService,IngredientService>();
        services.AddScoped<IPriceListService, PriceListService>();
        services.AddScoped<IOrderService, OrderService>();

        services.AddScoped<IUnitOfWork, UnitOfWork>();
        services.AddScoped<Lab3DbContext>();
    }
        
    public static void RegisterCustomValidators(this IServiceCollection services)
    {
        services.AddScoped<IValidator<NewDishDto>, CreateDishDtoValidator>();
        services.AddSingleton<IValidator<UpdateDishDto>, UpdateDishDtoValidator>();
        
        services.AddSingleton<IValidator<CreateIngredientDto>, CreateIngredientDtoValidator>();
        services.AddSingleton<IValidator<UpdateIngredientDto>, UpdateIngredientDtoValidator>();
        
        services.AddSingleton<IValidator<CreatePriceListDto>, CreatePriceListDtoValidator>();
        services.AddSingleton<IValidator<UpdatePriceListDto>, UpdatePriceListDtoValidator>();
        
        services.AddSingleton<IValidator<CreateOrderDto>, CreateOrderDtoValidator>();
        services.AddSingleton<IValidator<UpdateOrderDto>, UpdateOrderDtoValidator>();
    }

    public static void ConfigureCustomValidationErrors(this IServiceCollection services)
    {
        services.Configure<ApiBehaviorOptions>(options =>
        {
            options.InvalidModelStateResponseFactory = context =>
            {
                var errors = context.ModelState.Values.SelectMany(x => x.Errors.Select(p => p.ErrorMessage)).ToList();
                var result = new
                {
                    Message = "Validation errors",
                    Errors = errors
                };

                return new BadRequestObjectResult(result);
            };
        });
    }
    public static void RegisterAutoMapper(this IServiceCollection services)
    {
        services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<DishProfile>();
                cfg.AddProfile<IngredientProfile>();
                cfg.AddProfile<PriceListProfile>();
                cfg.AddProfile<OrderProfile>();
            },
            Assembly.GetExecutingAssembly());
    }
    
    }
}