﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Lab3.API.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _delegate;

        public ExceptionMiddleware(RequestDelegate aDelegate)
        {
            _delegate = aDelegate;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _delegate(context);
            }
            catch (Exception e)
            {
                await ProcessException(context, e);
            }
        }

        private static Task ProcessException(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            var errorCode = exception switch
            {
                KeyNotFoundException => 404,
                ArgumentException => 400,
                _ => 500,
            };
            context.Response.StatusCode = errorCode;
            var json = JsonSerializer.Serialize(new
            {
                error = errorCode == 500 ? "Internal server error" : exception.Message,
                code = errorCode
            });
            Console.WriteLine(exception);

            return context.Response.WriteAsync(json);
        }

    }
}