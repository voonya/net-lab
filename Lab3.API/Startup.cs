using BLL.Services.Dtos.Ingredient;
using FluentValidation.AspNetCore;
using Lab3.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Lab3.API.Extensions;
using Lab3.API.Middlewares;
using Lab3.API.Validation.Dish;
using Lab3.API.Validation.Ingredient;

namespace Lab3.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var migrationAssembly = typeof(Lab3DbContext).Assembly.GetName().Name;
            services.AddDbContext<Lab3DbContext>(options =>
                options.UseNpgsql(Configuration["ConnectionStrings:WebApiDatabase"],
                    opt => opt.MigrationsAssembly(migrationAssembly)));

            services.AddCors(o => o.AddPolicy("Client", builder =>
            {
                builder.WithOrigins("http://localhost:3000")
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));
            
            services.AddControllers();
            
            
            
            services.ConfigureCustomValidationErrors();
            services.AddFluentValidationAutoValidation();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Lab3.API", Version = "v1" });
            });

            services.RegisterAutoMapper();

            services.AddCors();

            services.RegisterCustomServices();
            
            services.RegisterCustomValidators();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Lab3.API v1"));
            }

            //app.UseHttpsRedirection();
            app.UseCors("Client");
            
            app.UseRouting();

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}