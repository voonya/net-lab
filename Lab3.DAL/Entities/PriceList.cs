﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Lab3.Entities.Abstract;

namespace Lab3.Entities
{
    public class PriceList: BaseEntity
    {
        public int DishId { get; set; }
        public Dish Dish { get; set; }
        
        [Range(0, Int32.MaxValue)]
        public int Weight { get; set; }
        
        [Range(0.0, Double.MaxValue)]
        public decimal Price { get; set; }
        
        public ICollection<Order> Orders { get; set; }
    }
}