﻿using System.Collections.Generic;
using Lab3.Entities.Abstract;

namespace Lab3.Entities
{
    public class Order : BaseEntity
    {
        public ICollection<PriceList> Items { get; set; }
    }
}