﻿using System;

namespace Lab3.Entities.Abstract
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}