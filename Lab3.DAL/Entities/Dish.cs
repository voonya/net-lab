﻿using System.Collections.Generic;
using Lab3.Entities.Abstract;

namespace Lab3.Entities
{
    public class Dish: BaseEntity
    {
        public string Name { get; set; }
        public ICollection<Ingredient> Ingredients { get; set; }
        public ICollection<PriceList> PriceLists { get; set; }
    }
}