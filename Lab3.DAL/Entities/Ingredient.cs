﻿using System.Collections.Generic;
using Lab3.Entities.Abstract;

namespace Lab3.Entities
{
    public class Ingredient: BaseEntity
    {
        public string Name { get; set; }
        public ICollection<Dish> Dishes { get; set; }
    }
}