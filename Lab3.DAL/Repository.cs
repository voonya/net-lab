﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab3.Context;
using Lab3.Entities.Abstract;
using Lab3.Interfaces;
using Microsoft.EntityFrameworkCore;



namespace Lab3
{
    public class Repository<T> 
    {
        /*
        protected readonly Lab3DbContext _context;

        public Repository(Lab3DbContext context)
        {
            _context = context;
        }

        virtual public Task<List<T>> GetAll(string includeProperties = "")
        {
            IQueryable<T> query = _context.Set<T>();
            
            foreach (var includeProperty in includeProperties.Split
                         (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            return query.ToListAsync();
        }

        virtual public Task<T?> GetById(int id, string includeProperties = "")
        {
            IQueryable<T> query = _context.Set<T>();
            
            foreach (var includeProperty in includeProperties.Split
                         (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            return query.FirstOrDefaultAsync(i => i.Id == id);
        }

        virtual public async Task Add(T entity)
        {
            _context.Set<T>().AddAsync(entity);
        }

        virtual public async Task Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        virtual public async Task DeleteById(int id)
        {
            T? entity = await GetById(id);
            await Delete(entity);
        }

        virtual public async Task Update(T entity)
        {
            _context.Set<T>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        virtual public Task<T?> GetByName(string name)
        {
            
        }
        */
    }
}