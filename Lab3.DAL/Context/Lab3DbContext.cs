﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Lab3.Entities;

namespace Lab3.Context
{
    public class Lab3DbContext: DbContext
    {
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<PriceList> PriceLists { get; set; }
        public DbSet<Order> Orders { get; set; }

        public Lab3DbContext(DbContextOptions<Lab3DbContext> options): base(options){}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Ingredient>()
                .HasIndex(u => u.Name)
                .IsUnique();
            
            modelBuilder
                .Entity<Dish>()
                .HasIndex(u => u.Name)
                .IsUnique();
            
            modelBuilder.Entity<Dish>()
                .HasMany(c => c.Ingredients)
                .WithMany(s => s.Dishes);

            modelBuilder.Entity<PriceList>()
                .HasOne<Dish>(s => s.Dish)
                .WithMany(s => s.PriceLists)
                .HasForeignKey(p => p.DishId)
                .OnDelete(DeleteBehavior.Restrict);
            
            modelBuilder.Entity<Order>()
                .HasMany(c => c.Items)
                .WithMany(s => s.Orders);
            
            modelBuilder.Entity<Dish>().Property(t => t.Name).HasMaxLength(200).IsRequired();

            modelBuilder.Entity<Ingredient>().Property(t => t.Name).HasMaxLength(200).IsRequired();

            modelBuilder.Entity<PriceList>().Property(t => t.DishId).IsRequired();

            modelBuilder.Entity<PriceList>().Property(t => t.Weight).IsRequired();

            modelBuilder.Entity<PriceList>().Property(t => t.Price).IsRequired().HasPrecision(15, 2);

        }
    }
}