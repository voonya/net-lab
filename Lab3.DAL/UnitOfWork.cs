﻿using System.Threading.Tasks;
using DAL.Interfaces;
using Lab3.Context;
using Lab3.Entities;
using Lab3.Interfaces;

namespace Lab3
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Lab3DbContext _lab3DbContext;

        public UnitOfWork(Lab3DbContext lab3DbContext)
        {
            _lab3DbContext = lab3DbContext;
            DishRepository = new DishRepository(lab3DbContext);
            IngredientRepository = new IngredientRepository(lab3DbContext);
            PriceListRepository = new PriceListRepository(lab3DbContext);
            OrdersRepository = new OrderRepository(lab3DbContext);
        }

        public IDishRepository DishRepository { get; }
        
        public IIngredientRepository IngredientRepository { get; }
        
        public IPriceListRepository PriceListRepository { get; }
        
        public IOrderRepository OrdersRepository { get; }

        public Task<int> SaveChangesAsync()
        {
            return _lab3DbContext.SaveChangesAsync();
        }
    }
}