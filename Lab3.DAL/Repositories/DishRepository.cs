﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using Lab3.Context;
using Lab3.Entities;
using Microsoft.EntityFrameworkCore;

namespace Lab3
{
    public class DishRepository : IDishRepository
    {
        private Lab3DbContext _context;
        public DishRepository(Lab3DbContext context)
        {
            _context = context;
        }
        
        virtual public Task<List<Dish>> GetAll()
        {
           
            return _context.Dishes.Include("Ingredients").ToListAsync();
        }

        virtual public Task<Dish?> GetById(int id)
        {
            return _context.Dishes.Include("Ingredients").FirstOrDefaultAsync(i => i.Id == id);
        }

        virtual public async Task Add(Dish entity)
        {
            _context.Dishes.AddAsync(entity);
        }

        virtual public async Task Delete(Dish entity)
        {
            _context.Dishes.Remove(entity);
        }

        virtual public async Task DeleteById(int id)
        {
            Dish? entity = await GetById(id);
            await Delete(entity);
        }

        virtual public async Task Update(Dish entity)
        {
            _context.Dishes.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public Task<Dish?> GetByName(string name)
        {
            return _context.Dishes.FirstOrDefaultAsync(p => p.Name == name);
        }
    }
}