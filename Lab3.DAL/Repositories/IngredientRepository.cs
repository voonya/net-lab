﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using Lab3.Context;
using Lab3.Entities;
using Microsoft.EntityFrameworkCore;

namespace Lab3
{
    public class IngredientRepository : IIngredientRepository
    {
        private Lab3DbContext _context;
        public IngredientRepository(Lab3DbContext context)
        {
            _context = context;
        }
        
        virtual public Task<List<Ingredient>> GetAll()
        {
           
            return _context.Ingredients.ToListAsync();
        }

        virtual public Task<Ingredient?> GetById(int id)
        {
            return _context.Ingredients.FirstOrDefaultAsync(i => i.Id == id);
        }

        virtual public async Task Add(Ingredient entity)
        {
            _context.Ingredients.AddAsync(entity);
        }

        virtual public async Task Delete(Ingredient entity)
        {
            _context.Ingredients.Remove(entity);
        }

        virtual public async Task DeleteById(int id)
        {
            Ingredient? entity = await GetById(id);
            await Delete(entity);
        }

        virtual public async Task Update(Ingredient entity)
        {
            _context.Ingredients.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public Task<Ingredient?> GetByName(string name)
        {
            return _context.Ingredients.FirstOrDefaultAsync(p => p.Name == name);
        }
    }
}