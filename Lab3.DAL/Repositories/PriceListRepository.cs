﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab3.Context;
using Lab3.Entities;
using Lab3.Entities.Abstract;
using Lab3.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Lab3
{
    public class PriceListRepository : IPriceListRepository
    {
        private Lab3DbContext _context;
        public PriceListRepository(Lab3DbContext context)
        {
            _context = context;
        }

        public Task<PriceList?> GetById(int id)
        {
            return _context.PriceLists.Include(p => p.Dish).ThenInclude(p => p.Ingredients).FirstOrDefaultAsync(i => i.Id == id);
        }

        public Task<List<PriceList>> GetAll()
        {
            return _context.PriceLists.Include(p => p.Dish).ThenInclude(p => p.Ingredients).ToListAsync();
        }
        
        public async Task Add(PriceList entity)
        {
            _context.PriceLists.AddAsync(entity);
        }

        public async Task Delete(PriceList entity)
        {
            _context.PriceLists.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
            PriceList? entity = await GetById(id);
            await Delete(entity);
        }

        public async Task Update(PriceList entity)
        {
            _context.PriceLists.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}