﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab3.Context;
using Lab3.Entities;
using Lab3.Entities.Abstract;
using Lab3.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Lab3
{
    public class OrderRepository : IOrderRepository
    {
        private Lab3DbContext _context;
        public OrderRepository(Lab3DbContext context)
        {
            _context = context;
        }

        public Task<Order?> GetById(int id)
        {
            return _context.Orders.Include(p => p.Items)
                .ThenInclude(p => p.Dish)
                .ThenInclude(p => p.Ingredients)
                .FirstOrDefaultAsync(i => i.Id == id);
        }

        public Task<List<Order>> GetAll()
        {
            return _context.Orders.Include(p => p.Items)
                .ThenInclude(p => p.Dish)
                .ThenInclude(p => p.Ingredients)
                .ToListAsync();
        }
        public async Task Add(Order entity)
        {
            _context.Orders.AddAsync(entity);
        }

        public async Task Delete(Order entity)
        {
            _context.Orders.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
            Order? entity = await GetById(id);
            await Delete(entity);
        }

        public async Task Update(Order entity)
        {
            _context.Orders.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }
        }
}