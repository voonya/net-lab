﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lab3.Entities;
using Lab3.Interfaces;

namespace Lab3
{
    public interface IOrderRepository : IRepository<Order>
    {
    }
}