﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lab3.Entities.Abstract;

#nullable enable

namespace Lab3.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        public Task<List<T>> GetAll();

        public Task<T?> GetById(int id);

        public Task Add(T entity);

        public Task Delete(T entity);

        public Task DeleteById(int id);

        public Task Update(T entity);
    }
}