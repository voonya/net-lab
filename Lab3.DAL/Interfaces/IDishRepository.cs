﻿using System.Threading.Tasks;
using Lab3.Entities;
using Lab3.Interfaces;

namespace DAL.Interfaces
{
    public interface IDishRepository : IRepository<Dish>
    {
        public Task<Dish?> GetByName(string name);
    }
}