﻿using System.Threading.Tasks;
using Lab3.Entities;
using Lab3.Interfaces;

namespace DAL.Interfaces
{
    public interface IIngredientRepository : IRepository<Ingredient>
    {
        public Task<Ingredient?> GetByName(string name);
    }
}