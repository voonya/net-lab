﻿using System.Threading.Tasks;
using DAL.Interfaces;
using Lab3.Entities;

namespace Lab3.Interfaces
{
    public interface IUnitOfWork
    {
        IDishRepository DishRepository { get; }
        IIngredientRepository IngredientRepository { get; }
        IPriceListRepository PriceListRepository { get; }
        
        IOrderRepository OrdersRepository { get; }
        
        //IRepository<PriceList> OrderListRepository { get; }
        Task<int> SaveChangesAsync();
    }
}